const { createApp } = Vue;
createApp({
  data() {
    return {
      randomIndex: 0,
      randomIndexInternet: 0,

      //vetor de imagens locais
      imagensLocais: [
        "./imagens/lua.jpg",
        "./imagens/SENAI_logo.png",
        "./imagens/sol.jpg",
      ],

        ImagensInternet:[
            'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg',
            'https://lasmagrelas.com.br/wp-content/uploads/2021/10/IMG_3692.png',
            'https://i.pinimg.com/originals/bb/16/5c/bb165c8fcecf107962691450d7505dd3.jpg',
        ],

    }; //fim return
  }, //Fim data

  computed: {
    randomImage() 
    {
      return this.imagensLocais[this.randomIndex];
    },//fim randomImage

    randomImagemInternet()
        {
        return this.ImagensInternet[this.randomIndexInternet];
        },//fim randomInternet
    }, //fim computed

  methods: {
    getRandomImage() 
    {
      this.randomIndex = Math.floor(Math.random() * this.imagensLocais.length);
      this.randomIndexInternet = Math.floor(Math.random() * this.ImagensInternet.length);

    },
  }, //fim methods


}).mount("#app");