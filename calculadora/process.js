const { createApp } = Vue;
createApp({
  data() {
    return {
      display: "0",
      operandoAtual: null,
      operadorAnterior: null,
      operador: null,
    } //Fechamento do return
  }, //Fechamento da função "data"

  methods: {
    handleButtonClick(botao) {
      console.log("teste");
      switch(botao) {
        case"+":
        case"-":
        case"/":
        case"*":
            this.handleOperation(botao);
            break;

        case ".":
            this.handleDecimal();
            break;

        case "=":
          this.handleEquals();
          break;

        default:
          this.handleNumber(botao);
          break;
      } //Fechamento switch
    }, //Fechamento handleButtonClick

    handleNumber(numero) {
        console.log("teste");
      if (this.display === "0") {
        this.display = numero.toString();
      } else {
        this.display += numero.toString();
        /*this.display = this.display + numero.toString(),*/
      }
    }, //Fechamento handleNumber
    handleOperation(operacao){
      if(this.operandoAtual !== null){
        this.handleEquals();
      }//Fechamento do if
        this.operador = operacao;

        this.operandoAtual = parseFloat(this.display);
        this.display = "0";

    },//FechamentohandleOperation

    handleEquals(){
        const displayAtual = parseFloat(this.display);
        if(this.operandoAtual !== null && this.operador !== null){
          switch(this.operador){
            case "+":
                this.display = (this.operandoAtual + displayAtual).toString();
                break;
                case "-":
                  this.display = (this.operandoAtual - displayAtual).toString();
                  break;
                  case "*":
                    this.displayAtual = (this.operandoAtual * displayAtual).toString();
                    break;
                    case "/":
                      this.display = (this.operandoAtual / displayAtual).toString();
                      break;
            }//Fim do switch

            this.operadorAnterior = this.operandoAtual;
            this.operandoAtual = null;
            this.operador = null;

        }//Fechamento do if
        else 
        {
            this.operandoAnterior = displayAtual;
        }//Fechamento do else

      }, //Fechamento handleEquals

      handleDecimal(){
        if(!this.display.includes("."))
        {
            this.display += ".";
        }//Fechamento if
      },//Fechamento handleDecimal

      handleClear()
      {
        this.display = "0";
        this.operandoAtual = null;
        this.operandoAnterior = null;
        this.operador = null;
      },//Fim handleClear
    
  }, //Fim methods
}).mount("#app"); //Fechamento do "createApp"
